package MessageToMe.demo.User;

import com.sun.istack.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.sql.Timestamp;
import java.time.LocalDateTime;

@Table(name="User")
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Data
@EntityListeners(AuditingEntityListener.class)
public class User {

    @Id
    @NotNull
    @GeneratedValue
    @Column(name = "userId", unique = true)
    private Long userId;

    // 여기 유니크키를 달아야 할까요?
    @Column(name = "userName", unique = true)
    private String userName;

    @NotNull
    @Column(name = "email", unique = true)
    private String email;

    @NotNull
    @Column(name = "password")
    private String password;

    @CreatedDate
    @Column(name = "createDate")
    private LocalDateTime createDate;

    @Column(name = "authNum")
    private Integer authNum; // 수정: int -> Integer

    //카카오 가입용
    @Column(name = "idToken")
    private String idToken;
}
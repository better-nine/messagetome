package MessageToMe.demo.User;

import MessageToMe.demo.Exception.ErrorMessage;
import MessageToMe.demo.Exception.ErrorResponse;
import MessageToMe.demo.User.dto.UserDTO;
import lombok.RequiredArgsConstructor;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.function.Supplier;

@Service
@Transactional(readOnly = true)
@RequiredArgsConstructor
public class UserService {
    private final UserRepository userRepository;
    private static final BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();

    /**
     * 회원가입(이메일) : 개인정보 저장
     * @param userDTO
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    public String save(UserDTO userDTO) {
        try{
            User user = new User();
            user.setPassword(encoder.encode(userDTO.getPassword())+"");
            user.setEmail(userDTO.getEmail());
            user.setUserName(userDTO.getUserName());
            userRepository.save(user);
        } catch (Exception e) {
            return "fail";
        }
        return "ok";
    }

    /**
     * 회원가입(카카오) : 개인정보 저장
     * @param userDTO
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    public UserDTO kakaoLogin(UserDTO userDTO) throws ErrorResponse {
        try{
            User user = userRepository.findUserByIdToken(userDTO.getIdToken()).orElseGet(
                    () -> kakaoSave(userDTO));
            userDTO.setUserName(user.getUserName());
            userDTO.setEmail(user.getEmail());
            userDTO.setUserId(user.getUserId());
            return userDTO;
        } catch (Exception e) {
            throw new ErrorResponse(ErrorMessage.SERVER);
        }
    }
    /**
     * 회원가입(카카오) : 개인정보 저장
     * @param userDTO
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    public User kakaoSave(UserDTO userDTO) {
        User user = new User();
        long n = userRepository.countAllKaKaoUserFromUser();
        user.setIdToken(userDTO.getIdToken());
        user.setUserId(userDTO.getUserId());
        if (userDTO.getUserName() == null) {
            user.setUserName("카카오에서 "+ n + "번째로 날아온 종이");
        } else {
            user.setUserName("카카오에서 "+ n + "번째로 날아온 " + userDTO.getUserName());
        }
        userRepository.save(user);
        return user;
    }

    /**
     * 카카오 회원 중복검사
     * @param idToken
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    public User selectKakao(String idToken){
        return userRepository.findUserByIdToken(idToken).orElse(null);
    }

    /**
     * 회원가입 : 이메일 중복검사
     * @param email
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    public String selectEmail(String email) {
        try{
            User user = userRepository.findUserByEmail(email).orElse(null);
            if (user != null){
                return "ALREADY_EXIST_EMAIL";
            }
            return "ok";
        } catch (Exception e){
            return "fail";
        }
    }

    /**
     * 회원가입 : 닉네임 중복검사
     * @param userName
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    public String selectUserName(String userName) {
        try{
            User user = userRepository.findUserByUserName(userName).orElse(null);
            if (user != null){
                return "ALREADY_EXIST_USER_NAME";
            }
            return "ok";
        } catch (Exception e){
            throw new RuntimeException("SERVER_ERROR");
        }
    }

    /**
     * 로그인 (이메일)
     * @param userDTO
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    public UserDTO login(UserDTO userDTO){
        try {
            User user = userRepository.findUserByEmail(userDTO.getEmail()).orElseThrow(() -> new IllegalArgumentException("notExist"));
            if (encoder.matches(userDTO.getPassword(), user.getPassword())) {
                userDTO.setUserName(user.getUserName());
                userDTO.setEmail(user.getEmail());
                userDTO.setUserId(user.getUserId());
                return userDTO;
            } else {
                throw new IllegalArgumentException("passwordError");
            }
        } catch (Exception e) {
            throw new RuntimeException("fail");
        }
    }

    /**
     * 개인 정보 수정 :: 비밀번호
     * @param userDTO
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    public UserDTO updatePassword(UserDTO userDTO){
        try{
            User user = userRepository.findUserByUserId(userDTO.getUserId()).orElseThrow(() -> new IllegalArgumentException("NOT_FOUND_USER"));
            if (user.getIdToken() != null) {
                throw new IllegalArgumentException("KAKAO_USER");
            }
            user.setPassword(encoder.encode(userDTO.getPassword())+"");
        }catch (Exception e){
            System.out.println(e);
            throw e;
        }
        return userDTO;
    }

    /**
     * 개인 정보 수정 :: 닉네임
     * @param userDTO
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    public UserDTO updateName(UserDTO userDTO){
        try{
            // 중복검사
            User user = userRepository.findUserByUserName(userDTO.getUserName()).orElse(null);
            if (user != null) {
                throw new IllegalArgumentException("ALREADY_EXIST_USER_NAME");
            }
            user = userRepository.findUserByUserId(userDTO.getUserId()).orElseThrow(() -> new IllegalArgumentException("NOT_FOUND_USER"));
            user.setUserName(userDTO.getUserName());
        } catch (Exception e){
            System.out.println(e);
            throw e;
        }
        return userDTO;
    }

    /**
     * 회원 탈퇴
     * @param userDTO
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    public String delete(UserDTO userDTO){
        try{
            User user = userRepository.findUserByUserId(userDTO.getUserId()).orElseThrow(() -> new IllegalArgumentException("notExist"));
            userRepository.delete(user);
        }catch (Exception e){
            throw new RuntimeException("fail");
        }
        return "success";
    }


}

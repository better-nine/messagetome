package MessageToMe.demo.Interceptor;

import MessageToMe.demo.User.JWTService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Map;

@Slf4j
//@Component 
public class MessageToMeInterceptor implements HandlerInterceptor {

    @Autowired
    static JWTService jwtService;

    public MessageToMeInterceptor(JWTService jwtService) {
        this.jwtService = jwtService;
    }

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        log.info(">> interceptor start");
        boolean preHandleResult = false;

        // ip세션 저장 (나중에 쓸 예정 > )
        // 접속 유저정보 : 저장해야하는데 테이블 어케하죠 고민이되네요
        String userIp = request.getRemoteAddr();
        System.out.println("user ip : " + userIp);

        String header = request.getHeader(HttpHeaders.AUTHORIZATION);
        System.out.println(header);
/*      토큰검사 잠시 패스
        Map<String, Object> verifyJWT = jwtService.verifyJWT(request.getHeader("token"));
        if(verifyJWT != null) {
            System.out.println("j : " + verifyJWT.toString() );
            preHandleResult = true;
        } else {
            System.out.println("j : " + verifyJWT.toString() );
        }*/

        preHandleResult = true;
        //true > controller 진입
        //false > controller 진입불가
        return preHandleResult;
    }

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {
        HandlerInterceptor.super.postHandle(request, response, handler, modelAndView);
    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {
        HandlerInterceptor.super.afterCompletion(request, response, handler, ex);
    }
}

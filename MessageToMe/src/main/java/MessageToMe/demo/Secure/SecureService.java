package MessageToMe.demo.Secure;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional(readOnly = true)
@RequiredArgsConstructor
public class SecureService {

    private final SecureRepository secureRepository;

    /**
     * 보안키값 리턴
     * @param key
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    public String selectSecureKey(String key) {
        String code = "";
        try{
            code = secureRepository.findSecureBySecureKey(key).getSecureCode();
        } catch (Exception e) {
            code = "fail";
        }
        return code;
    }
}

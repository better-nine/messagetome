package MessageToMe.demo.Message;

import MessageToMe.demo.User.User;
import MessageToMe.demo.Paper.Paper;
import lombok.*;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.GenerationTime;
import org.hibernate.internal.build.AllowPrintStacktrace;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;
import org.springframework.validation.annotation.Validated;

import javax.persistence.*;
import javax.validation.Valid;
import javax.validation.constraints.Size;
import javax.validation.executable.ValidateOnExecution;
import java.sql.Date;
import java.sql.Timestamp;
import java.time.Clock;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@Table(name="Message")
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Data
@EntityListeners(AuditingEntityListener.class)
@Validated
public class Message {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "messageId")
    private long messageId;

    @Column(columnDefinition = "TEXT", length = 420, nullable = false)
    @Size(min=1)
    @Size(max=420)
    private String content;

    @Column
    private String font;

    @Column
    private String color;

    @Column
    private char readyn;

    @Column
    private char delyn;

    @Column(name = "create_date")
    @CreatedDate
    @CreationTimestamp
    private LocalDateTime createdate;

    @Column(name = "delete_date")
    private LocalDateTime deletedate;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "userId")
    private User user;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "paperId")
    private Paper paper;

    @Builder
    public Message(String content, String font, String color, char readyn, char delyn, User user, Paper paper){
        this.content = content;
        this.font = font;
        this.color = color;
        this.readyn = readyn;
        this.delyn = delyn;
        this.user = user;
        this.paper = paper;
    }

    public void update(String content, String font, String color){
        this.content = content;
        this.font = font;
        this.color = color;
    }

    public void delete(char delyn){
        LocalDateTime localDateTime = LocalDateTime.now();
        Timestamp timestamp = Timestamp.valueOf(localDateTime);
        this.deletedate = timestamp.toLocalDateTime();
        this.delyn = delyn;
    }
}

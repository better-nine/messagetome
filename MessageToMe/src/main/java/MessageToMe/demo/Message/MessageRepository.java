package MessageToMe.demo.Message;

import MessageToMe.demo.Paper.Paper;
import MessageToMe.demo.User.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Repository
public interface MessageRepository extends JpaRepository<Message, Long> {
    Message findMessageByPaperAndUser(Paper paper, User user);
    Optional<Message> findMessageByMessageId(Long messageId);
    @Transactional
    void deleteByMessageId(Long messageId);
    Optional<List<Message>> findAllByUserAndDelynOrderByCreatedateDesc(User user, char delyn);
    Optional<Message> findMessageByMessageIdAndDelyn(Long messageId, char delyn);
    Optional<List<Message>> findAllByPaperAndDelyn(Paper paper, char delyn);
    Optional<List<Message>> findAllByPaperAndDelynOrderByCreatedateDesc(Paper paper, char delyn);
    Optional<List<Message>> findTop5ByPaperAndDelynOrderByCreatedateDesc(Paper paper, char delyn);

    @Modifying(clearAutomatically = true)
    @Query (value = "update Message m set m.readyn = ?2 where m.paper = ?1")
    void updateMessageReadyn(Paper paper, char readyn);

    @Query(value = "select m from Message m join fetch m.paper where m.user = :user and m.delyn = 'n' and m.paper.delyn='n'")
    Optional<List<Message>> findAllWithPaperTitle(User user);
}
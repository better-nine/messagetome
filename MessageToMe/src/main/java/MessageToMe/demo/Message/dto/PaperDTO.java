package MessageToMe.demo.Message.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Locale;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PaperDTO {
    private Long paperId;
    private String paperTitle;
}

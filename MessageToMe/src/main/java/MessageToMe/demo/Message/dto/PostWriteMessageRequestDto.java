package MessageToMe.demo.Message.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.validation.annotation.Validated;

import javax.validation.Valid;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Validated
public class PostWriteMessageRequestDto {
    UserDTO user;
    PaperDTO paper;
    @Valid
    MessageDTO message;
}

package MessageToMe.demo.Message.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class StickerDTO {
    private Long stickerId;
    private String positionX;
    private String positionY;
    private Long kind;
    private Long userId;
    private Long paperId;
    private String userName;
}


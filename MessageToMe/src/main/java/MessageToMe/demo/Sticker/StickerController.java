package MessageToMe.demo.Sticker;

import MessageToMe.demo.Exception.ErrorResponse;
import MessageToMe.demo.Sticker.dto.*;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@Slf4j
@RestController
@AllArgsConstructor
public class StickerController {

    private final StickerService stickerService;

    @PostMapping("/sticker")
    public ResponseEntity<?> createSticker(@RequestBody RequestStickerDTO requestStickerDTO) {
        try {
            long userId = requestStickerDTO.getUser().getUserId();
            long paperId = requestStickerDTO.getPaper().getPaperId();

            StickerDTO stickerDTO = new StickerDTO();
            stickerDTO.setPaperId(paperId);
            stickerDTO.setKind(requestStickerDTO.getSticker().getKind());
            stickerDTO.setPositionX(requestStickerDTO.getSticker().getPositionX());
            stickerDTO.setPositionY(requestStickerDTO.getSticker().getPositionY());

            Long saveStickerId = stickerService.saveSticker(userId, stickerDTO).getStickerId();

            ResponseStickerIdDTO responseStickerIdDTO = new ResponseStickerIdDTO(new StickerIdDTO(saveStickerId));

            return new ResponseEntity(responseStickerIdDTO, HttpStatus.OK);
        } catch (ErrorResponse e) {
            log.error(e.getMessage());
            return new ResponseEntity(e.getErrorMessage().getMessage(),
                    HttpStatus.valueOf(e.getErrorMessage().getStatus()));
        }
    }

    @DeleteMapping("/sticker/{stickerId}")
    public ResponseEntity<?> deleteSticker(@PathVariable("stickerId") String stickerId, @RequestBody DeleteRequestDTO deleteRequestDTO) {
        try {
            long userId = deleteRequestDTO.getUser().getUserId();
            long paperId = deleteRequestDTO.getPaper().getPaperId();
            long parseStickerId = Long.parseLong(stickerId);

            StickerDTO stickerDTO = new StickerDTO();
            stickerDTO.setStickerId(parseStickerId);
            stickerDTO.setPaperId(paperId);

            stickerService.deleteSticker(userId, stickerDTO);

            return new ResponseEntity("SUCCESS", HttpStatus.OK);
        } catch (ErrorResponse e) {
            log.error(e.getMessage());
            return new ResponseEntity(e.getErrorMessage().getMessage(),
                    HttpStatus.valueOf(e.getErrorMessage().getStatus()));
        }
    }

    @PutMapping("/sticker/{stickerId}")
    public ResponseEntity<?> updateSticker(@PathVariable("stickerId") String stickerId, @RequestBody RequestStickerDTO requestStickerDTO) {
        try {
            long userId = requestStickerDTO.getUser().getUserId();
            long paperId = requestStickerDTO.getPaper().getPaperId();
            long parseStickerId = Long.parseLong(stickerId);

            StickerDTO stickerDTO = new StickerDTO();
            stickerDTO.setStickerId(parseStickerId);
            stickerDTO.setPaperId(paperId);
            stickerDTO.setKind(requestStickerDTO.getSticker().getKind());
            stickerDTO.setPositionX(requestStickerDTO.getSticker().getPositionX());
            stickerDTO.setPositionY(requestStickerDTO.getSticker().getPositionY());

            stickerService.updateSticker(userId, stickerDTO);

            return new ResponseEntity("SUCCESS", HttpStatus.OK);
        } catch (ErrorResponse e) {
            log.error(e.getMessage());
            return new ResponseEntity(e.getErrorMessage().getMessage(),
                    HttpStatus.valueOf(e.getErrorMessage().getStatus()));
        }
    }

}

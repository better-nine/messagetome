package MessageToMe.demo.Sticker;

import MessageToMe.demo.Paper.Paper;
import MessageToMe.demo.User.User;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;

@Table(name="Sticker")
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Data
@EntityListeners(AuditingEntityListener.class)
public class Sticker {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "stickerId")
    private long stickerId;

    @Column(name="positionX")
    private String positionX;

    @Column(name="kind")
    private long kind;

    @Column(name="positionY")
    private String positionY;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "userId")
    private User user;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "paperId")
    private Paper paper;

}

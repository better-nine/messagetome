package MessageToMe.demo.Sticker;

import MessageToMe.demo.Exception.ErrorMessage;
import MessageToMe.demo.Exception.ErrorResponse;
import MessageToMe.demo.Paper.Paper;
import MessageToMe.demo.Paper.PaperRepository;
import MessageToMe.demo.Sticker.dto.StickerDTO;
import MessageToMe.demo.User.User;
import MessageToMe.demo.User.UserRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional (readOnly = true)
@RequiredArgsConstructor
@Slf4j
public class StickerService {

    private final StickerRepository stickerRepository;
    private final PaperRepository paperRepository;
    private final UserRepository userRepository;

    @Transactional
    public Sticker saveSticker(long userId, StickerDTO stickerDTO) throws ErrorResponse {
        User findUser = userRepository.findUserByUserId(userId)
                .orElseThrow(()-> new ErrorResponse(ErrorMessage.NOT_FOUND_USER));
        Paper findPaper = paperRepository.findPaperByPaperId(stickerDTO.getPaperId())
                .orElseThrow(()-> new ErrorResponse(ErrorMessage.NOT_FOUND_PAPER));
        try {

            Sticker sticker = new Sticker();
            sticker.setKind(stickerDTO.getKind());
            sticker.setPositionX(stickerDTO.getPositionX());
            sticker.setPositionY(stickerDTO.getPositionY());
            sticker.setPaper(findPaper);
            sticker.setUser(findUser);

            validateDuplicateSticker(findUser, findPaper);
            checkStickerCountOnPaper(findPaper);

            return stickerRepository.save(sticker);
        } catch (Exception e){
            log.error(e.getMessage());
            throw new ErrorResponse(ErrorMessage.DB_SAVING);
        }
    }

    private void checkStickerCountOnPaper(Paper paper) throws ErrorResponse {
        int stickerCount = stickerRepository.countStickerByPaper(paper);
        if (stickerCount >= 140) {
            throw new ErrorResponse(ErrorMessage.TOO_MANY_EXISTING_STICKERS);
        }
    }

    private void validateDuplicateSticker(User user, Paper paper) throws ErrorResponse {
        boolean isExistSticker = stickerRepository.existsStickerByUserAndPaper(user, paper);
        if (isExistSticker) {
            throw new ErrorResponse(ErrorMessage.ALREADY_EXIST_STICKER);
        }
    }

    @Transactional
    public ResponseEntity deleteSticker(long userId, StickerDTO stickerDTO) throws ErrorResponse {
        User findUser = userRepository.findUserByUserId(userId)
                .orElseThrow(()-> new ErrorResponse(ErrorMessage.NOT_FOUND_USER));
        Paper findPaper = paperRepository.findPaperByPaperId(stickerDTO.getPaperId())
                .orElseThrow(() -> new ErrorResponse(ErrorMessage.NOT_FOUND_PAPER));
        Sticker findSticker = stickerRepository.findStickerByStickerId(stickerDTO.getStickerId())
                .orElseThrow(() -> new ErrorResponse(ErrorMessage.NOT_FOUND_STICKER));

        if(!findSticker.getUser().getUserId().equals(findUser.getUserId())) {
            throw new ErrorResponse(ErrorMessage.NOT_VALID_USER);
        }

        if(!findSticker.getPaper().getPaperId().equals(findPaper.getPaperId())) {
            throw new ErrorResponse(ErrorMessage.NOT_FOUND_STICKER);
        }

        try {
            stickerRepository.delete(findSticker);
            return new ResponseEntity("SUCCESS", HttpStatus.OK);
        } catch (Exception e) {
            log.error(e.getMessage());
            throw new ErrorResponse(ErrorMessage.SERVER);
        }
    }

    @Transactional
    public ResponseEntity updateSticker(long userId, StickerDTO stickerDTO) throws ErrorResponse {
        User findUser = userRepository.findUserByUserId(userId)
                .orElseThrow(()-> new ErrorResponse(ErrorMessage.NOT_FOUND_USER));
        Paper findPaper = paperRepository.findPaperByPaperId(stickerDTO.getPaperId())
                .orElseThrow(() -> new ErrorResponse(ErrorMessage.NOT_FOUND_PAPER));
        Sticker findSticker = stickerRepository.findStickerByStickerId(stickerDTO.getStickerId())
                .orElseThrow(() -> new ErrorResponse(ErrorMessage.NOT_FOUND_STICKER));

        if(!findSticker.getUser().getUserId().equals(findUser.getUserId())) {
            throw new ErrorResponse(ErrorMessage.NOT_VALID_USER);
        }

        if(!findSticker.getPaper().getPaperId().equals(findPaper.getPaperId())) {
            throw new ErrorResponse(ErrorMessage.NOT_FOUND_STICKER);
        }

        try {
            findSticker.setKind(stickerDTO.getKind());
            findSticker.setPositionX(stickerDTO.getPositionX());
            findSticker.setPositionY(stickerDTO.getPositionY());
            return new ResponseEntity("SUCCESS", HttpStatus.OK);
        } catch (Exception e) {
            log.error(e.getMessage());
            throw new ErrorResponse(ErrorMessage.DB_SAVING);
        }

    }

}

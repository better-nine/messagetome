package MessageToMe.demo.HealthCheck;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin("*")
public class HealthCheckController {

    @GetMapping(path = "/")
    public ResponseEntity<?> healthCheck(){
        return new ResponseEntity<>("SUCCESS", HttpStatus.OK);
    }
}

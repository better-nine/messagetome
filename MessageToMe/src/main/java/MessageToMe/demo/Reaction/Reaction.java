package MessageToMe.demo.Reaction;

import MessageToMe.demo.Message.Message;
import MessageToMe.demo.Paper.Paper;
import MessageToMe.demo.User.User;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.sql.Timestamp;
import java.time.LocalDateTime;

@Table(name="Reaction")
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Data
@EntityListeners(AuditingEntityListener.class)
public class Reaction {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "reactionId")
    private long reactionId;

    @Column
    private String emoji;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "userId")
    private User user;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "messageId")
    private Message message;

    @Builder
    public Reaction(String emoji, User user, Message message){
        this.emoji = emoji;
        this.user = user;
        this.message = message;
    }

    public void update(String emoji){
        this.emoji = emoji;
    }
}

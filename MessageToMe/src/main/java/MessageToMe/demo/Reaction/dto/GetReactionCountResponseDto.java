package MessageToMe.demo.Reaction.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class GetReactionCountResponseDto {
    private int reactionCnt;
}

package MessageToMe.demo.Reaction;

import MessageToMe.demo.Exception.ErrorMessage;
import MessageToMe.demo.Exception.ErrorResponse;
import MessageToMe.demo.Message.Message;
import MessageToMe.demo.Message.MessageRepository;
import MessageToMe.demo.Reaction.dto.GetReactionCountResponseDto;
import MessageToMe.demo.User.User;
import MessageToMe.demo.User.UserRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional(readOnly = true)
@RequiredArgsConstructor
@Slf4j
public class ReactionService {

    private final ReactionRepository reactionRepository;
    private final MessageRepository messageRepository;
    private final UserRepository userRepository;

    public Long save(String emoji, Long userId, Long messageId) throws ErrorResponse{
        User user = userRepository.findUserByUserId(userId)
                .orElseThrow(() -> new ErrorResponse(ErrorMessage.NOT_FOUND_USER));
        Message message = messageRepository.findMessageByMessageId(messageId)
                .orElseThrow(() -> new ErrorResponse(ErrorMessage.NOT_FOUND_PAPER));
        try {
            Reaction reaction = Reaction.builder()
                    .user(user)
                    .message(message)
                    .emoji(emoji)
                    .build();
            System.out.println(reaction);
            Long savedId = reactionRepository.save(reaction).getReactionId();
            return savedId;
        } catch (Exception e){
            log.error(String.valueOf(e));
            throw new ErrorResponse(ErrorMessage.DB_SAVING);
        }
    }

    @Transactional
    public ResponseEntity update(Long userId, Long reactionId, String emoji) throws ErrorResponse{
        User user = userRepository.findUserByUserId(userId)
                .orElseThrow(() -> new ErrorResponse(ErrorMessage.NOT_FOUND_USER));
        Reaction reaction = reactionRepository.findReactionByReactionId(reactionId)
                .orElseThrow(() -> new ErrorResponse(ErrorMessage.NOT_FOUND_REACTION));

        // user의 이메일이 리액션 write의 이메일과 일치하지 않는 경우
        if (user.getUserId() != reaction.getUser().getUserId()){
            throw new ErrorResponse(ErrorMessage.NOT_VALID_USER);
        }

        try{
           reaction.update(emoji);
           return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e){
            log.error(String.valueOf(e));
            throw new ErrorResponse(ErrorMessage.DB_SAVING);
        }
    }

    @Transactional
    public ResponseEntity delete(Long userId, Long reactionId) throws ErrorResponse{
        User user = userRepository.findUserByUserId(userId)
                .orElseThrow(() -> new ErrorResponse(ErrorMessage.NOT_FOUND_USER));
        Reaction reaction = reactionRepository.findReactionByReactionId(reactionId)
                .orElseThrow(() -> new ErrorResponse(ErrorMessage.NOT_FOUND_REACTION));

        // user의 이메일이 리액션 write의 이메일과 일치하지 않는 경우
        if (user.getUserId() != reaction.getUser().getUserId()){
            throw new ErrorResponse(ErrorMessage.NOT_VALID_USER);
        }

        try{
            reactionRepository.deleteReactionByReactionId(reactionId);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e){
            log.error(String.valueOf(e));
            throw new ErrorResponse(ErrorMessage.DB_SAVING);
        }
    }

    public GetReactionCountResponseDto getReactionCnt(Long messageId) throws ErrorResponse{
        Message message = messageRepository.findMessageByMessageId(messageId)
                .orElseThrow(() -> new ErrorResponse(ErrorMessage.NOT_FOUND_MESSAGE));

        int ReactionCount = reactionRepository.findReactionByMessage(message)
                .orElseThrow(() -> new ErrorResponse(ErrorMessage.NOT_FOUND_REACTION)).size();

        try {
            GetReactionCountResponseDto getReactionCountResponseDto = new GetReactionCountResponseDto();
            getReactionCountResponseDto.setReactionCnt(ReactionCount);
            return getReactionCountResponseDto;
        } catch (Exception e){
           log.error(String.valueOf(e));
           throw new ErrorResponse(ErrorMessage.SERVER);
        }
    }
}

package MessageToMe.demo.Reaction;


import MessageToMe.demo.Exception.ErrorResponse;
import MessageToMe.demo.Reaction.dto.DeleteReactionDto;
import MessageToMe.demo.Reaction.dto.GetReactionCountResponseDto;
import MessageToMe.demo.Reaction.dto.PostWritReactionRequestDto;
import MessageToMe.demo.Reaction.dto.PutUpdateReactionRequestDto;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.method.P;
import org.springframework.web.bind.annotation.*;

//@CrossOrigin("*")
@RestController
@AllArgsConstructor
@Slf4j
public class ReactionController {
    private final ReactionService reactionService;

    @PostMapping(path = "/reaction")
    public ResponseEntity<?> writeReaction(@RequestBody PostWritReactionRequestDto postWritReactionRequestDto){
        try{
            Long savedId = reactionService.save(
                    postWritReactionRequestDto.getReaction().getEmoji(),
                    postWritReactionRequestDto.getUser().getUserId(),
                    postWritReactionRequestDto.getMessage().getMessageId()
            );
            return new ResponseEntity<>("SUCCESS", HttpStatus.CREATED);
        }
        catch (ErrorResponse e){
            log.error(e.getMessage());
            return new ResponseEntity<>(e.getErrorMessage().getMessage(),
                    HttpStatus.valueOf(e.getErrorMessage().getStatus()));
        }
    }

    @PutMapping(path = "/reaction/{reactionId}")
    public ResponseEntity<?> updateReaction(@PathVariable String reactionId, @RequestBody PutUpdateReactionRequestDto putUpdateReactionRequestDto){
        try{
            Long parseReactionId = Long.parseLong(reactionId);
            reactionService.update(
                    putUpdateReactionRequestDto.getUser().getUserId(),
                    parseReactionId,
                    putUpdateReactionRequestDto.getReaction().getEmoji()
            );
            return new ResponseEntity<>("SUCCESS", HttpStatus.CREATED);
        }
        catch (ErrorResponse e){
            log.error(e.getMessage());
            return new ResponseEntity<>(e.getErrorMessage().getMessage(),
                    HttpStatus.valueOf(e.getErrorMessage().getStatus()));
        }
    }

    @DeleteMapping(path = "/reaction/{reactionId}")
    public ResponseEntity<?> deleteReaction(@PathVariable String reactionId, @RequestBody DeleteReactionDto deleteReactionDto){
        try {
            Long parseReactionId = Long.parseLong(reactionId);
            reactionService.delete(
                    deleteReactionDto.getUser().getUserId(),
                    parseReactionId
            );
            return new ResponseEntity<>("SUCCESS", HttpStatus.CREATED);
        }
        catch (ErrorResponse e){
            log.error(e.getMessage());
            return new ResponseEntity<>(e.getErrorMessage().getMessage(),
                    HttpStatus.valueOf(e.getErrorMessage().getStatus()));
        }
    }

    @GetMapping(path = "/reaction/{messageId}")
    public ResponseEntity<?> getReactionCnt(@PathVariable Long messageId){
        try {
            GetReactionCountResponseDto getReactionCountResponseDto  = reactionService.getReactionCnt(messageId);
            return new ResponseEntity<>(getReactionCountResponseDto, HttpStatus.OK);
        }
        catch (ErrorResponse e){
            log.error(e.getMessage());
            return new ResponseEntity<>(e.getErrorMessage().getMessage(),
                    HttpStatus.valueOf(e.getErrorMessage().getStatus()));
        }
    }

}

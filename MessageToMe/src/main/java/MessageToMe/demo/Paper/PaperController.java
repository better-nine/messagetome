package MessageToMe.demo.Paper;

import MessageToMe.demo.Exception.ErrorResponse;
import MessageToMe.demo.Paper.dto.*;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.List;

@Slf4j
@RestController
@AllArgsConstructor
public class PaperController {

    private final PaperService paperService;

    @PostMapping("/paper")
    public ResponseEntity<?> createPaper(@RequestBody RequestPaperAndUserDTO requestPaperAndUserDTO) {
        try {
            long userId = requestPaperAndUserDTO.getUser().getUserId();
            PaperDTO paperDTO = requestPaperAndUserDTO.getPaper();
            paperDTO.setCreateDate(LocalDateTime.now());
            long paperId = paperService.savePaper(userId, paperDTO).getPaperId();
            ResponseCreatePaperDTO responseCreatePaperDTO = new ResponseCreatePaperDTO(new PaperIdDTO(paperId));
            return new ResponseEntity<>(responseCreatePaperDTO, HttpStatus.OK);
        } catch (ErrorResponse e) {
            log.error(e.getMessage());
            return new ResponseEntity<>(e.getErrorMessage().getMessage(),
                    HttpStatus.valueOf(e.getErrorMessage().getStatus()));
        }
    }

    @GetMapping("/paper/todayPaper")
    public ResponseEntity<?> validateCreateDate(@RequestHeader("User-Id") long userId) {
        try {
            boolean result = paperService.validateCreateDate(userId);
            // true : 페이퍼 생성 이력 있음, false : 페이퍼 생성 이력 없음
            // front 리턴시에는 true : 생성 가능, false : 생성 불가능
            return new ResponseEntity<>(!result, HttpStatus.OK);
        } catch (ErrorResponse e) {
            log.error(e.getMessage());
            return new ResponseEntity<>(e.getErrorMessage().getMessage(),
                    HttpStatus.valueOf(e.getErrorMessage().getStatus()));
        }
    }

    @GetMapping("/paper")
    public ResponseEntity<?> getPaperList(@RequestHeader("User-Id") long userId, Pageable pageable) {
        try {
            List<PaperListDTO> paperListDTOList = paperService.getPaperList(userId, pageable);
            ResponseGetPaperListDTO responseGetPaperListDTO = new ResponseGetPaperListDTO(paperListDTOList);
            return new ResponseEntity<>(responseGetPaperListDTO, HttpStatus.OK);
        } catch (ErrorResponse e) {
            log.error(e.getMessage());
            return new ResponseEntity<>(e.getErrorMessage().getMessage(),
                    HttpStatus.valueOf(e.getErrorMessage().getStatus()));
        }
    }

    @PutMapping("/paper/{paperId}")
    public ResponseEntity<?> updatePaper(@PathVariable("paperId") String paperId, @RequestBody RequestPaperAndUserDTO requestPaperAndUserDTO) {
        try {
            long userId = requestPaperAndUserDTO.getUser().getUserId();
            PaperDTO paperDTO = requestPaperAndUserDTO.getPaper();
            paperDTO.setPaperId(Long.parseLong(paperId));
            paperService.updatePaper(userId, paperDTO);
            return new ResponseEntity<>("SUCCESS", HttpStatus.OK);
        } catch (ErrorResponse e) {
            log.error(e.getMessage());
            return new ResponseEntity<>(e.getErrorMessage().getMessage(),
                    HttpStatus.valueOf(e.getErrorMessage().getStatus()));
        }
    }

    @DeleteMapping("/paper/{paperId}")
    public ResponseEntity<?> deletePaper(@PathVariable("paperId") String paperId, @RequestBody RequestUserDTO requestUserDTO) {
        try {
            long userId = requestUserDTO.getUser().getUserId();
            Long parsePaperId = Long.parseLong(paperId);
            paperService.deletePaper(userId, parsePaperId);
            return new ResponseEntity<>("SUCCESS", HttpStatus.OK);
        } catch (ErrorResponse e) {
            log.error(e.getMessage());
            return new ResponseEntity<>(e.getErrorMessage().getMessage(),
                    HttpStatus.valueOf(e.getErrorMessage().getStatus()));
        }
    }

    @PostMapping("/paper/{paperId}")
    public ResponseEntity<?> paperDetailList(@PathVariable("paperId") String paperId, @RequestBody RequestUserDTO requestUserDTO) {
        try {
            long userId = requestUserDTO.getUser().getUserId();
            long parsePaperId = Long.parseLong(paperId);
            return new ResponseEntity<>(paperService.getPaperDetail(userId, parsePaperId), HttpStatus.OK);
        } catch (ErrorResponse e) {
            log.error(e.getMessage());
            return new ResponseEntity<>(e.getErrorMessage().getMessage(),
                    HttpStatus.valueOf(e.getErrorMessage().getStatus()));
        }
    }

    @GetMapping("/paper/{paperId}")
    public ResponseEntity<?> paperDetailList(@PathVariable("paperId") String paperId) {
        try {
            long parsePaperId = Long.parseLong(paperId);
            return new ResponseEntity<>(paperService.getPaperDetail(parsePaperId), HttpStatus.OK);
        } catch (ErrorResponse e) {
            log.error(e.getMessage());
            return new ResponseEntity<>(e.getErrorMessage().getMessage(),
                    HttpStatus.valueOf(e.getErrorMessage().getStatus()));
        }
    }

    @PutMapping("/paper/gift/{paperId}")
    public ResponseEntity<?> sendPaper(@PathVariable("paperId") String paperId, @RequestBody RequestGiftDTO requestGiftDTO) {
        try {
            long userId = requestGiftDTO.getUser().getUserId();
            String recipientName = requestGiftDTO.getRecipient().getUserName();
            long parsePaperId = Long.parseLong(paperId);
            paperService.sendPaper(userId, recipientName, parsePaperId);
            return new ResponseEntity<>("SUCCESS", HttpStatus.OK);
        } catch (ErrorResponse e) {
            log.error(e.getMessage());
            return new ResponseEntity<>(e.getErrorMessage().getMessage(),
                    HttpStatus.valueOf(e.getErrorMessage().getStatus()));
        }
    }

}

package MessageToMe.demo.Paper;

import MessageToMe.demo.User.User;
import com.sun.istack.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.DynamicInsert;

import javax.persistence.*;
import java.time.LocalDateTime;

@DynamicInsert
@NoArgsConstructor
@Entity
@Data
@AllArgsConstructor
public class Paper {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "paperId")
    private Long paperId;

    @NotNull
    @Column(name = "paperTitle", length = 50)
    private String paperTitle;

    @Column(name = "paperSkin")
    private int paperSkin;

    @Column(name = "delyn")
    @NotNull
    private char delyn;

    @Column(name = "createDate")
    private LocalDateTime createDate;

    @Column(name = "deleteDate")
    private LocalDateTime deleteDate;

//    페이퍼 생성자
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "createUserId")
    private User createUser;

//    페이퍼 소유자
//    페이퍼 생성시에 페이퍼 생성자의 User 엔티티가 들어가게 함
//    선물하기가 y일 때만 선물 받는 사람의 User 엔티티가 들어가게 함
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ownerUserId")
    private User ownerUser;

    @Column(name = "giftyn")
    private char giftyn;

}

package MessageToMe.demo.Paper.dto;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor (access = AccessLevel.PROTECTED)
public class ResponsePaperDetailListDTO {
    PaperDetailDTO papers;
    List<MessageDTO> messages;
    List<StickerDTO> stickers;
    List<ReactionDTO> reactions;
}

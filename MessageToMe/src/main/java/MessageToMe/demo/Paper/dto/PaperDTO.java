package MessageToMe.demo.Paper.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@AllArgsConstructor
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class PaperDTO {

    private Long paperId;
    private String paperTitle;
    private int skin;
    @JsonFormat(pattern = "yyyy-MM-dd")
    private LocalDateTime createDate;
    private char giftyn;

    public PaperDTO(String paperTitle, int skin, LocalDateTime createDate) {
        this.paperTitle = paperTitle;
        this.skin = skin;
        this.createDate = createDate;
    }
}

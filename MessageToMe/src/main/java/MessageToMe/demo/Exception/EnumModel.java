package MessageToMe.demo.Exception;

public interface EnumModel {
    String getMessage();
    int getStatus();
}

package MessageToMe.demo.Exception;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@Data
public class ErrorResponse extends Throwable{
    private ErrorMessage errorMessage;

    public ErrorResponse(ErrorMessage errorMessage) {
        this.errorMessage = errorMessage;
    }
}

# 별말, 씀

모바일 웹 기반 롤링 페이퍼 서비스 "**별말,씀**" 백엔드 애플리케이션

## 서비스 주소
https://www.byeolmal.today/

## 개요
1. 인원 
   - 3명

2. 기간 
   - 2022.02.12 ~ (진행중)

3. 소개 
   - Spring Boot 기반 롤링페이퍼 서비스 API 구현

4. 목표 
   - Spring Boot, Spring Data JPA, Spring Security를 사용하여 개발부터 배포까지의 과정을 경험하며 스프링의 동작방식, 쿼리문, 웹에 대하여 중점적으로 학습

### 만든 이
#### @RoxyYujinKim
- Team Leader
- DevOps
- Message API, Reaction API

#### @better-nine
- User API

#### @tein408
- Paper API, Sticker API

### API 문서
[POSTMAN](https://documenter.getpostman.com/view/15855367/UVkvJXf8)

[구글시트](https://docs.google.com/spreadsheets/d/e/2PACX-1vR8syuC6DG2Lm_oTAweqgm36aiRttI237qbzlIqRj9XLZ0jHt2gYgntZY18Wb2sgidqrCTHlRF2DNSM/pubhtml)

### 기술 스택
`Back-end`
- Java 11
- Spring Boot 
- Spring Data JPA
- Spring Security
- Maven

`DataBase`
- MariaDB

### 개발 환경
- OS : Mac os / window 10
- Server : AWS EC2

### 주요 기능
- Spring Security를 이용하여 비밀번호 암호화
- JWT토큰으로 로그인 상태 유지
- `페이퍼` 선물 하기
- `메시지` 좋아요 기능

### 설치
1. 프로젝트 클론
```
$ git clone https://gitlab.com/jamtomer/messagetome.git
```

2. 메이븐 빌드
```
$ mvn compile package
```

3. 도커 이미지 빌드
```
$ docker build -t byeolmaltoday .
```

4. 도커 이미지 실행
```
$ sudo docker run -it -d -p 80:8000 --name byeolmaltoday byeolmaltoday
```


### 노션
- https://best-cobra-fb9.notion.site/MessageToMe-6435598d99634b4d88de9983ea12df43
